execute as @s run scoreboard players set @s cooldown 0
execute as @s run scoreboard players set @s is_racing 0
execute as @s if data entity @s {SelectedItem:{id:"minecraft:paper",tag:{display:{Name:'["",{"text":"Lapping Pass","italic":false,"color":"yellow"}]'}}}} run scoreboard players set @s _Ticks 0
execute as @s if data entity @s {SelectedItem:{id:"minecraft:paper",tag:{display:{Name:'["",{"text":"Lapping Pass","italic":false,"color":"yellow"}]'}}}} run scoreboard players set @s is_racing 1
execute as @s unless data entity @s {Inventory:[{id:"minecraft:paper",tag:{display:{Name:'["",{"text":"Lapping Pass","italic":false,"color":"yellow"}]'}}}]} run give @s paper{display:{Name:'["",{"text":"Lapping Pass","italic":false,"color":"yellow"}]',Lore:['["",{"text":"Holding this item while crossing","italic":false}]','["",{"text":"a gate will cause you to lap,","italic":false}]','["",{"text":"rather than finish.","italic":false}]']}}

tellraw @s [{"text":"Finish! "}, {"text":"(","color":"gray"}, {"score":{"name":"@s","objective":"Minutes"}}, {"text":"m","color":"gray"}, {"score":{"name":"@s","objective":"Seconds"}}, {"text":"s","color":"gray"}, {"score":{"name":"@s","objective":"Milliseconds"}}, {"text":"ms) [","color":"gray"}, {"text":"Share","clickEvent":{"action":"run_command","value":"/trigger Share"},"hoverEvent":{"action":"show_text","contents":"(You can only share your last time, not any prior (sorry))"}}, {"text":"]","color":"gray"}]
scoreboard players enable @s Share
