execute as @s if block ~ ~ ~ minecraft:tripwire if score @s cooldown <= Math cooldown run playsound minecraft:block.note_block.harp block @s
execute as @s at @s if block ~ ~ ~ minecraft:tripwire if score @s cooldown <= Math cooldown if entity @s[scores={is_racing=0}] run function fpv_gates:start_race
execute as @s at @s if block ~ ~ ~ minecraft:tripwire if score @s cooldown <= Math cooldown if entity @s[scores={is_racing=1}] run function fpv_gates:stop_race
