scoreboard objectives add is_racing dummy
scoreboard objectives add _Ticks dummy
scoreboard objectives add Ticks dummy
scoreboard objectives add Milliseconds dummy
scoreboard objectives add Seconds dummy
scoreboard objectives add Minutes dummy
scoreboard players set @a is_racing 0
scoreboard players set @a _Ticks 0
scoreboard players set @a Ticks 0
scoreboard players set @a Milliseconds 0
scoreboard players set @a Seconds 0
scoreboard players set @a Minutes 0
scoreboard objectives add ticks_in_sec dummy
scoreboard players set Math ticks_in_sec 20
scoreboard objectives add ms_in_tick dummy
scoreboard players set Math ms_in_tick 50
scoreboard objectives add sec_in_min dummy
scoreboard players set Math sec_in_min 60
scoreboard objectives add cooldown dummy
scoreboard players set @a cooldown 0
scoreboard players set Math cooldown -30
scoreboard objectives add Share trigger
scoreboard players set @a Share 0
scoreboard objectives add AllowGamemode dummy