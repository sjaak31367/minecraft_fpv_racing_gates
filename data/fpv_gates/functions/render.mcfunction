scoreboard players operation @s Ticks = @s _Ticks
scoreboard players operation @s Ticks %= Math ticks_in_sec
scoreboard players operation @s Milliseconds = @s Ticks
scoreboard players operation @s Milliseconds *= Math ms_in_tick
scoreboard players operation @s Seconds = @s _Ticks
scoreboard players operation @s Seconds /= Math ticks_in_sec
scoreboard players operation @s Seconds %= Math sec_in_min
scoreboard players operation @s Minutes = @s _Ticks
scoreboard players operation @s Minutes /= Math ticks_in_sec
scoreboard players operation @s Minutes /= Math sec_in_min

title @s actionbar [{"text":"Lap time: ","color":"gray"}, {"score":{"name":"@s","objective":"Minutes"},"color":"white"}, {"text":"m","color":"gray"}, {"score":{"name":"@s","objective":"Seconds"},"color":"white"}, {"text":"s","color":"gray"}, {"score":{"name":"@s","objective":"Milliseconds"},"color":"white"}, {"text":"ms","color":"gray"}]
