execute as @a unless entity @s[scores={is_racing=1}] run scoreboard players set @s is_racing 0
execute as @a if entity @s[scores={is_racing=1}] run scoreboard players add @s _Ticks 1
execute as @a run function fpv_gates:render
execute as @a at @s if block ~ ~ ~ minecraft:tripwire run function fpv_gates:gate_pass
scoreboard players remove @a cooldown 1
function fpv_gates:shares
execute as @a[scores={AllowGamemode=1}] at @s if block ~ ~-1 ~ minecraft:brown_shulker_box run function fpv_gates:gamemodes

#scoreboard objectives setdisplay sidebar _Ticks
