# Minecraft FPV Racing Gates

#### Description
A Minecraft datapack which adds a lap-timer or course-timer for racing FPV drones (though really any racing would work).  
Originally made for use with [this](https://www.curseforge.com/minecraft/mc-mods/fpv-drone) FPV mod (though not required).

#### Installing
Either `git clone https://gitlab.com/sjaak31367/minecraft_fpv_racing_gates.git` into `.minecraft/saves/_world_/datapacks/`, or place the .zip of this repository there.

#### Using
To create a gate, simply make a (2D) shape with an opening (e.g. portal-frame, circle, heart-shape).  
And fill the "hole" (area which you wish the user to pass through) with string.  
Then simply pass through it, and the race will start.  
Pass through another gate (or the same) for it to finish timing your lap and end the race (unless you have selected lapping mode).  
(To enable lapping mode, simply hold a "Lapping Pass" in your hand (you'll be provided one after finishing your first race).)  
  
And to switch between spectator mode and creative mode, simply place down a brown shulker box, and stand atop it.  
(By default this is disabled, to allow a player to do this, grant them access via `/scoreboard players set @p AllowGamemode 1`, if you want everybody to be able to change gamemodes like this, set up a command block at spawn executing that.)

#### Misc.
This datapack was _not_ made with the intent of being an be-all and end-all solution, with hyperadvanced features that will be updated extensively.
It's simple and it works for my purposes. I could maybe add things like gate-progression tracking, or highlighting, or automatic tracking of best time, comparing to other racers, different-track modes, etc. But this suffices.  
It was made for Minecraft 1.16.5, it might break on older/newer versions due to commands being added or removed from the game.

#### License
note: If you use it, a name/link somewhere would be nice, but not needed.  
This work by Sjaak van Montfort and contributors is uses license: [Creative Commons CC0 1.0 Universal (CC-0)](https://creativecommons.org/publicdomain/zero/1.0/).
